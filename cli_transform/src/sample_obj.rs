use nalgebra::{Isometry3, Point3, Vector3};
use ncollide3d::{
    shape::TriMesh,
    query::RayCast,
    query::ray_internal::Ray,
    query::visitors::RayInterferencesCollector,
    partitioning::BVH
};
use ndarray::Array3;
use rayon::prelude::*;

use std::f32;

// TODO: this function is more efficient if the longest axis of the object is
// placed along the x axis. Maybe add some transformation to optimize
// automatically
pub fn sample_obj(object: &TriMesh<f32>, resolution: usize) -> Array3<f32> {
    let aabb = object.aabb();
    let extents = aabb.extents();
    let min_side_length = extents.x.min(extents.y).min(extents.z);
    let grid_scale = resolution as f32 / min_side_length;
    let grid_size = (extents * grid_scale).map(|c| c.ceil() as i32);

    let sub_grid_factor = 16;
    let sub_grid_scale = grid_scale * sub_grid_factor as f32;

    // Add some margin to the grid (useful in later calculations)
    let margin = 1;
    let full_grid_size = grid_size + Vector3::new(margin * 2, margin * 2, margin * 2);
    let mut voxel_grid = Array3::from_elem(
        (full_grid_size.x as usize, full_grid_size.y as usize, full_grid_size.z as usize),
        0.0
    );

    println!(
        "Using a grid size of: ({}, {}, {})",
        full_grid_size.x,
        full_grid_size.y,
        full_grid_size.z
    );

    // Dedicated thread to report sampling progress
    let (sender, receiver) = std::sync::mpsc::sync_channel(grid_size.z as usize);
    std::thread::spawn(move|| {
        for i in 0 .. grid_size.z {
            let () = receiver.recv().unwrap();
            println!("Sampling: scanning {}/{}", i + 1, grid_size.z);
        }
    });

    let z_range = 0 .. (grid_size.z * sub_grid_factor);
    let raycast_results: Vec<Vec<_>> = z_range.into_par_iter().flat_map(|z| {
        let sender = sender.clone();
        let z_grid = z / sub_grid_factor;

        if z % sub_grid_factor == 0 {
            sender.send(()).unwrap();
        }
        (0..(grid_size.y * sub_grid_factor)).into_par_iter().map(move |y| {
            let y_grid = y / sub_grid_factor;

            let sub_grid_pos =
                Vector3::new(0.0, y as f32 + 0.5, z as f32 + 0.5) / sub_grid_scale;
            let ray = Ray::new(aabb.mins() + sub_grid_pos, Vector3::x());

            // Get all triangles whose bounding box intersect with the ray
            let mut interferences = Vec::new();
            object.bvt().visit(
                &mut RayInterferencesCollector::new(&ray, &mut interferences)
            );

            // Filter out the triangles where the ray actually intersects the
            // triangle itself
            let mut colliding_positions: Vec<_> = interferences.iter().filter_map(|&index| {
                let tri = object.triangle_at(index);
                tri.toi_with_ray(&Isometry3::identity(), &ray, false)
            }).collect();
            // Validate amount of ray intersections
            if colliding_positions.len() % 2 != 0 {
                panic!("Odd number of ray intersections! Is the mesh not manifold?");
            }

            colliding_positions.sort_by(|x1, x2| x1.partial_cmp(x2).unwrap());
            colliding_positions.chunks_exact(2).flat_map(move |chunk| {
                let (start_x, end_x) = (chunk[0] * grid_scale, chunk[1] * grid_scale);

                (start_x as i32 ..= end_x as i32).map(move |x| {
                    let size_x = if (x as f32) < start_x {
                        x as f32 + 1.0 - start_x
                    } else if end_x as i32 == x {
                        end_x - x as f32
                    } else {
                        1.0
                    };

                    // Offset index position by margin
                    let ipos = Point3::new(x, y_grid, z_grid) +
                        Vector3::new(margin, margin, margin);

                    (ipos, size_x / sub_grid_factor.pow(2) as f32)
                })
            }).collect()
        })
    }).collect::<Vec<_>>();

    for collisions in raycast_results {
        for (ipos, vol) in collisions {
            voxel_grid[(ipos.x as usize, ipos.y as usize, ipos.z as usize)] += vol;
        }
    }

    voxel_grid
}
