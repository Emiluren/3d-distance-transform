    // let errors = calculate_sphere_errors(&distance_map, SPHERE_SIZE);
    // //print_grid(&errors);
    // let max_error = max_abs_value(&errors.data);
    // let errors_iter = errors.data.iter().cloned();
    // let sum_error: f32 = errors_iter.clone().map(f32::abs).sum();
    // println!(
    //     "max error {}, average error {}, error range {}",
    //     max_error,
    //     sum_error / errors.data.len() as f32,
    //     errors_iter.clone().fold(f32::NEG_INFINITY, f32::max) -
    //         errors_iter.clone().fold(f32::INFINITY, f32::min)
    // );
    //errors.map(|v| dm_color(v / max_error * 255.0)).save_as_pngs("error");

    // let edge_cases = Grid3D::with_size_par_generate(
    //     coverage_samples.size,
    //     |pos| {
    //         let edge_dist = integer_distance[pos];
    //         let edge = pos + edge_dist;
    //         let coverage = coverage_samples[edge];

    //         let float_edge_dist = edge_dist.map(|c| c as f32);

    //         let default_dir = if float_edge_dist.magnitude() == 0.0 {
    //             Vector3::new(0.0, 0.0, 0.0)
    //         } else {
    //             float_edge_dist.normalize()
    //         };

    //         let gradient_normal = gradient[edge].normalize();
    //         let gradient_distance = edgedf(gradient_normal, coverage);

    //         let case = if gradient_distance.abs() <= 0.75_f32.sqrt() && float_edge_dist.cross(&gradient_normal).magnitude() <= 0.5 {
    //             edgedf_case(gradient_normal, coverage)
    //         } else {
    //             edgedf_case(default_dir, coverage)
    //         };

    //         case
    //     }
    // );

    // let mut case_errors_map = std::collections::HashMap::new();
    // case_errors_map.insert(EdgeDfCase::Linear, Vec::new());
    // case_errors_map.insert(EdgeDfCase::V1, Vec::new());
    // case_errors_map.insert(EdgeDfCase::V2, Vec::new());
    // case_errors_map.insert(EdgeDfCase::V3, Vec::new());
    // case_errors_map.insert(EdgeDfCase::V4, Vec::new());
    // case_errors_map.insert(EdgeDfCase::V5, Vec::new());
    // case_errors_map.insert(EdgeDfCase::V6, Vec::new());
    // for pos in ScanIterator::new((Axis::X, true), edge_cases.size) {
    //     case_errors_map.get_mut(&edge_cases[pos]).unwrap().push(
    //         errors[pos].abs()
    //     );
    // }

    // use EdgeDfCase::*;
    // for case in &[Linear, V1, V2, V3, V4, V5, V6] {
    //     println!(
    //         "{:?} {}",
    //         case,
    //         case_errors_map[case].iter().sum::<f32>() / case_errors_map[case].len() as f32
    //     );
    // }

    //println!("{:?}", case_errors_map[&EdgeDfCase::Linear]);

