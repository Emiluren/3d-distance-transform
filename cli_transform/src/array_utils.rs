use nalgebra::{Point3, Vector3};
use nalgebra::base::dimension::{U1, U3};
#[cfg(feature = "polygon-visualize")]
use isosurface::source::Source;
use ndarray::{Array3, ArrayBase, Ix3};
use hdf5::types::H5Type;
//use structopt::StructOpt;

use std::f32;
#[cfg(feature = "polygon-visualize")]
use std::cmp::{min, max};
use std::path::Path;

// #[derive(StructOpt)]
// pub struct DatasetOptions {
//     filename: String,
//     dataset_name: String,
// }

// impl FromStr for DatasetOptions {
// }

pub trait HdfWritable {
    fn hdf_write(&self, filename: &str, dataset_name: &str) -> hdf5::Result<()>;
}

// TODO: Seems unable to read files saved from octave
pub fn hdf_read<T: H5Type>(filename: &str, dataset_name: &str) -> hdf5::Result<Array3<T>> {
    let file = hdf5::File::open(filename, "r")?;
    let file_samples = file.dataset(dataset_name)?;
    file_samples.read()
}

impl<S: ndarray::Data<Elem = T>, T: H5Type> HdfWritable for ArrayBase<S, Ix3> {
    fn hdf_write(&self, filename: &str, dataset_name: &str) -> hdf5::Result<()> {
        // Append if the file exists, create a new one otherwise
        // this is supposed to work automatically with only "a" according to the hdf5_rust docs
        // but it prints an error message
        let file = if Path::new(filename).exists() {
            hdf5::File::open(filename, "a")?
        } else {
            hdf5::File::open(filename, "w")?
        };

        if file.link_exists(dataset_name) {
            // TODO: Add user confirm prompt
            println!("Dataset {} already exist in {}. Deleting it", dataset_name, filename);
            file.unlink(dataset_name)?;
        }

        let file_samples = file.new_dataset::<T>().create(dataset_name, self.dim())?;
        file_samples.write(self)
    }
}

pub fn calculate_sphere_errors<S: ndarray::Data<Elem = f32>>(
    grid: &ArrayBase<S, Ix3>, radius: usize, sphere_center: Point3<f32>
) -> Array3<f32> {
    let center = Vector3::from_iterator_generic(
        U3, U1,
        grid.shape().iter().map(
            |&i| i as f32 / 2.0
        )
    );
    Array3::from_shape_fn(
        grid.dim(),
        |(x, y, z)| {
            let pos = Point3::new(x as f32 + 0.5, y as f32 + 0.5, z as f32 + 0.5) - center;
            let dist = nalgebra::distance(&pos, &sphere_center) - radius as f32;
            grid[(x, y, z)] - dist
        }
    )
}

// TODO: Update save_as_pngs to use ndarray
#[cfg(feature = "export-images")]
impl Grid3D<u8> {
    pub fn save_as_pngs(&self, filename: &str) {
        for z in 0..self.size.z {
            let range_start = z * (self.size.x * self.size.y);
            let range_end = (z + 1) * (self.size.x * self.size.y);
            image::save_buffer(
                format!("{}_{}.png", filename, z),
                &self.data[range_start as usize .. range_end as usize],
                self.size.x as u32,
                self.size.y as u32,
                image::Gray(8)
            ).unwrap();
        }
    }
}

#[cfg(feature = "export-images")]
impl Grid3D<[u8; 3]> {
    pub fn save_as_pngs(&self, filename: &str) {
        for z in 0..self.size.z {
            let range_start = z * (self.size.x * self.size.y);
            let range_end = (z + 1) * (self.size.x * self.size.y);

            let flat_data: Vec<_> = self.data[
                range_start as usize .. range_end as usize
            ].iter().flatten().cloned().collect();

            image::save_buffer(
                format!("{}_{}.png", filename, z),
                &flat_data,
                self.size.x as u32,
                self.size.y as u32,
                image::RGB(8)
            ).unwrap();
        }
    }
}

// #[cfg(feature = "polygon-visualize")]
// pub trait DistanceGrid {
//     fn size(&self) -> (usize, usize, usize);
//     fn index(&self, pos: (usize, usize, usize)) -> f32;
// }

// #[cfg(feature = "polygon-visualize")]
// impl DistanceGrid for &Array3<f32> {
//     fn size(&self) -> (usize, usize, usize) {
//         self.dim()
//     }

//     fn index(&self, pos: (usize, usize, usize)) -> f32 {
//         self[pos]
//     }
// }

#[cfg(feature = "polygon-visualize")]
pub struct DistanceGrid {
    distance: Array3<f32>,
    //direction: Grid3D<Vector3<i32>>,
}

#[cfg(feature = "polygon-visualize")]
impl DistanceGrid {
    pub fn new(
        distance: Array3<f32>,
        //direction: Grid3D<Vector3<i32>>
    ) -> DistanceGrid {
        DistanceGrid {
            distance,
            //direction
        }
    }
}

#[cfg(feature = "polygon-visualize")]
fn cubic_interpolate(t: f32, values: [f32; 4]) -> f32 {
    values[1] + 0.5 * t*(
        values[2] - values[0] + t*(
            2.0*values[0] - 5.0*values[1] + 4.0*values[2] - values[3] + t*(
                3.0*(values[1] - values[2]) + values[3] - values[0])
        )
    )
}

#[cfg(feature = "polygon-visualize")]
fn bicubic_interpolate(tx: f32, ty: f32, values: [[f32; 4]; 4]) -> f32 {
    let mut intermediate_values = [0.0; 4];
    for x in 0..4 {
        intermediate_values[x] = cubic_interpolate(ty, values[x]);
    }
    cubic_interpolate(tx, intermediate_values)
}

#[cfg(feature = "polygon-visualize")]
fn tricubic_interpolate(tx: f32, ty: f32, tz: f32, values: [[[f32; 4]; 4]; 4]) -> f32 {
    let mut intermediate_values = [0.0; 4];
    for x in 0..4 {
        intermediate_values[x] = bicubic_interpolate(ty, tz, values[x]);
    }
    cubic_interpolate(tx, intermediate_values)
}

#[cfg(feature = "polygon-visualize")]
impl Source for DistanceGrid {
    // (x, y, z) should be guaranteed to be between (0, 0, 0) and (1, 1, 1)
    fn sample(&self, x: f32, y: f32, z: f32) -> f32 {
        //println!("{} {} {}", x, y, z);
        let (size_x, size_y, size_z) = self.distance.dim();
        let max_size = size_x.max(size_y).max(size_z) as f32;
        let sampling_pos = Vector3::new(x, y, z) * max_size;
        let (sx, sy, sz) = (sampling_pos.x as i32, sampling_pos.y as i32, sampling_pos.z as i32);

        let surrounding_xs = [sx - 1, sx, sx + 1, sx + 2];
        let surrounding_ys = [sy - 1, sy, sy + 1, sy + 2];
        let surrounding_zs = [sz - 1, sz, sz + 1, sz + 2];

        // TODO: use ndarray's slice indexing maybe
        let mut surrounding_values = [[[0.0; 4]; 4]; 4];
        for xi in 0..4 {
            for yi in 0..4 {
                for zi in 0..4 {
                    let x = surrounding_xs[xi];
                    let y = surrounding_ys[yi];
                    let z = surrounding_zs[zi];

                    // Clamp surrounding coordinates to edge
                    let clamped_x = max(0, min(size_x as i32 - 1, x));
                    let clamped_y = max(0, min(size_y as i32 - 1, y));
                    let clamped_z = max(0, min(size_z as i32 - 1, z));

                    // Extrapolate distance if out of bounds
                    let extra_distance = if (x, y, z) != (clamped_x, clamped_y, clamped_z) {
                        let dx = clamped_x - x;
                        let dy = clamped_y - y;
                        let dz = clamped_z - z;

                        ((dx*dx + dy*dy + dz*dz) as f32).sqrt()
                    } else {
                        0.0
                    };

                    let pos = (
                        clamped_x as usize,
                        clamped_y as usize,
                        clamped_z as usize
                    );
                    surrounding_values[xi][yi][zi] = (self.distance[pos] + extra_distance) / max_size;
                }
            }
        }

        let (tx, ty, tz) = (
            sampling_pos.x - sx as f32,
            sampling_pos.y - sy as f32,
            sampling_pos.z - sz as f32
        );
        let dist = tricubic_interpolate(tx, ty, tz, surrounding_values);

        dist
    }
}

// TODO: add normal interpolation for a HermiteSource implementation
