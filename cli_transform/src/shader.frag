#version 330
in vec3 vNormal;

layout(location=0) out vec4 color;

vec3 hemisphere(vec3 normal) {
    const vec3 light = vec3(0.1, -1.0, 0.0);
    float NdotL = dot(normal, light)*0.5 + 0.5;
    return mix(vec3(0.886, 0.757, 0.337), vec3(0.518, 0.169, 0.0), NdotL);
}

void main() {
    color = vec4(hemisphere(normalize(vNormal)), 1.0);
}
