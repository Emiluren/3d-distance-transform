#[cfg(feature = "polygon-visualize")]

mod array_utils;
mod sample_sphere;
#[cfg(feature = "obj")]
mod sample_obj;
#[cfg(feature = "polygon-visualize")]
mod render_polygonized;

use nalgebra::Point3;
#[cfg(feature = "obj")]
use ncollide3d::shape::TriMesh;
use structopt::StructOpt;

use std::path::Path;
use std::num::ParseFloatError;

use array_utils::HdfWritable;
#[cfg(feature = "polygon-visualize")]
use array_utils::DistanceGrid;
use distance_transform_3d::{distance_transform, SampleArray, default_coverage_to_distance};
use sample_sphere::sample_sphere;
#[cfg(feature = "obj")]
use sample_obj::sample_obj;
#[cfg(feature = "polygon-visualize")]
use isosurface::source::CentralDifference;

fn parse_point3(s: &str) -> Result<Point3<f32>, ParseFloatError> {
    let coords: Vec<&str> = s.split(',').collect();

    let x = coords[0].parse::<f32>()?;
    let y = coords[1].parse::<f32>()?;
    let z = coords[2].parse::<f32>()?;

    Ok(Point3::new(x, y, z))
}

// TODO: add subcommand for saving pngs
#[derive(StructOpt)]
#[structopt(name = "rename_all", rename_all = "kebab-case")]
enum TransformOptions {
    /// Calculates volume coverage for a perfect sphere
    SampleSphere {
        radius: usize,
        output_file: String,
        /// Output dataset name
        #[structopt(long, default_value = "coverage")]
        dataset: String,
        /// Offset the center of the sampled sphere in the format x,y,z
        #[structopt(long, default_value = "0,0,0", parse(try_from_str = "parse_point3"))]
        offset: Point3<f32>
    },
    #[cfg(feature = "obj")]
    /// Calculates volume coverage for a .obj 3d model
    SampleObj {
        obj_file: String,
        /// Resolution of the smallest side of the grid that the object is sampled in
        #[structopt(long, default_value = "16")]
        resolution: usize,
        output_file: String,
        /// Output dataset name
        #[structopt(long, default_value = "coverage")]
        output_dataset: String,
    },
    /// Applies distance transform to coverage data
    Transform {
        input_file: String,
        /// Input dataset name
        #[structopt(long, default_value = "coverage")]
        input_dataset: String,
        /// File to write distance to, defaults to input_file
        #[structopt(long)]
        output_file: Option<String>,
        /// Output dataset name, defaults to "distance"
        #[structopt(long, default_value = "distance")]
        output_dataset: String,
        // TODO: Add option for integer distance
    },
    /// Calculates distance map error compared to a sphere of some radius
    CalculateSphereError {
        input_file: String,
        #[structopt(long, default_value = "distance")]
        input_dataset: String,
        radius: usize,
        /// Offset the center of the sampled sphere in the format x,y,z
        #[structopt(long, default_value = "0,0,0", parse(try_from_str = "parse_point3"))]
        offset: Point3<f32>,
        /// File to write distance to, defaults to input_file
        #[structopt(long)]
        output_file: Option<String>,
        /// Output dataset name, defaults to "distance"
        #[structopt(long, default_value = "difference")]
        output_dataset: String,
    },
    #[cfg(feature = "polygon-visualize")]
    /// Creates a mesh from a distance map using marching cubes and render it
    RenderPolygonized {
        file: String,
        #[structopt(long, default_value = "distance")]
        dataset: String,
        /// Marching cubes resolution
        #[structopt(long, default_value = "6")]
        marching_cubes_resolution: usize,
    }
}

fn chunk_points<T: nalgebra::Scalar>(v: &Vec<T>) -> Vec<Point3<T>> {
    v.chunks_exact(3).map(|chunk| Point3::new(chunk[0], chunk[1], chunk[2])).collect()
}

use TransformOptions::*;
fn main() {
    // libhdf5 will print errors for example if a file is created in append mode
    // let _ = hdf5::silence_errors(); // Doesn't seem to help

    match TransformOptions::from_args() {
        SampleSphere { radius, output_file, dataset, offset } => {
            println!("Sampling coverage");
            let coverage_samples = sample_sphere(radius, offset);

            println!("Saving data");
            coverage_samples.hdf_write(&output_file, &dataset).unwrap();
        },
        #[cfg(feature = "obj")]
        SampleObj { obj_file, resolution, output_file, output_dataset } => {
            let mesh = &tobj::load_obj(
                &Path::new(&obj_file)
            ).unwrap().0[0].mesh;

            let positions = chunk_points(&mesh.positions);
            let indices = chunk_points(
                &mesh.indices.iter().map(|&p| p as usize).collect()
            );

            let ncollide_mesh = TriMesh::new(positions, indices, None);

            println!("Sampling coverage");
            let coverage_samples = sample_obj(&ncollide_mesh, resolution);

            coverage_samples.hdf_write(&output_file, &output_dataset).unwrap();
        },
        Transform { input_file, input_dataset, output_file, output_dataset } => {
            let coverage_samples = array_utils::hdf_read(
                &input_file, &input_dataset
            ).unwrap();

            println!("Calculating gradient");
            let gradient = coverage_samples.calculate_gradient();

            println!("Applying distance transform");
            let (distance_map, _integer_distance) =
                distance_transform(&coverage_samples, &gradient, default_coverage_to_distance);

            let output_file = output_file.unwrap_or(input_file.clone());
            distance_map.hdf_write(&output_file, &output_dataset).unwrap();
        },
        CalculateSphereError { input_file, input_dataset, radius, offset, output_file, output_dataset } => {
            let input_data: ndarray::Array3<f32> =
                array_utils::hdf_read(&input_file, &input_dataset).unwrap();

            let output_file = output_file.unwrap_or(input_file.clone());
            array_utils::calculate_sphere_errors(&input_data, radius, offset)
                .hdf_write(&output_file, &output_dataset).unwrap();
        },
        #[cfg(feature = "polygon-visualize")]
        RenderPolygonized { file, dataset, marching_cubes_resolution } => {
            let distance_map = array_utils::hdf_read(&file, &dataset).unwrap();

            let (grid_size_x, grid_size_y, grid_size_z) = distance_map.dim();
            let grid_max_size = grid_size_x.max(grid_size_y).max(grid_size_z) as f32;
            let center = Point3::new(
                0.5 * (grid_size_x as f32) / grid_max_size,
                0.5 * (grid_size_y as f32) / grid_max_size,
                0.5 * (grid_size_z as f32) / grid_max_size,
            );
            let marching_cube_source = CentralDifference::new(Box::new(
                DistanceGrid::new(distance_map)
            ));

            render_polygonized::render(
                &marching_cube_source, center, marching_cubes_resolution
            );
        }
    }
}
