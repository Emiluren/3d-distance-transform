use nalgebra::{Point3, Vector3};
use ndarray::prelude::*;
use std::f32;

pub fn sample_sphere(radius: usize, sphere_center: Point3<f32>) -> Array3<f32> {
    let subdivisions = [
        Vector3::new(0.0, 0.0, 0.0),
        Vector3::new(0.5, 0.0, 0.0),
        Vector3::new(0.0, 0.5, 0.0),
        Vector3::new(0.0, 0.0, 0.5),
        Vector3::new(0.5, 0.5, 0.0),
        Vector3::new(0.0, 0.5, 0.5),
        Vector3::new(0.5, 0.0, 0.5),
        Vector3::new(0.5, 0.5, 0.5),
    ];

    let sub_voxel_coverage = SubVoxelCoverage {
        f: &|sub_voxel_coverage, pos1, sub_voxel_side| {
            let pos2 = pos1 + Vector3::from_element(sub_voxel_side);

            let center_dist1 = pos1 - sphere_center;
            let center_dist2 = pos2 - sphere_center;

            let min_dist = Vector3::zip_map(
                &center_dist1,
                &center_dist2,
                |x1, x2| f32::min(x1.abs(), x2.abs())
            );
            let max_dist = Vector3::zip_map(
                &center_dist1,
                &center_dist2,
                |x1, x2| f32::max(x1.abs(), x2.abs())
            );

            const SUBDIVISION_EPSILON: f32 = 1.0 / 128.0;
            // Check if the subvoxel sticks out of the sphere.
            // SUBDIVISION_EPSILON is the allowed amount that the subvoxel is allowed
            // to stick in or out while still counting as entirely on one side, otherwise
            // this would recurse infinitely.
            if min_dist.magnitude() - radius as f32 > -SUBDIVISION_EPSILON {
                // If both corners are outside this subvoxel is outside
                0.0
            } else if max_dist.magnitude() - (radius as f32) < SUBDIVISION_EPSILON {
                // If both corners are outside this subvoxel is inside
                sub_voxel_side.powi(3)
            } else {
                // Call recursively for subdivided sub voxel
                subdivisions.iter().map(
                    |v| sub_voxel_coverage.calc(pos1 + v * sub_voxel_side, sub_voxel_side / 2.0)
                ).sum()
            }
        }
    };

    let margin = 2;
    let half_aabb_size = radius + margin;
    let aabb_size = half_aabb_size * 2;
    let aabb_center = Vector3::from_element(half_aabb_size as f32);
    let mut prev_x = -1;

    Array3::from_shape_fn(
        (aabb_size, aabb_size, aabb_size),
        |(x, y, z)| {
            // Print sampling progress
            if x as isize != prev_x {
                println!("Sampling: x = {}/{}", x + 1, aabb_size);
                prev_x = x as isize;
            }

            let pos = Point3::new(x as f32, y as f32, z as f32) - aabb_center;
            sub_voxel_coverage.calc(pos, 1.0)
        }
    )
}

struct SubVoxelCoverage<'s> {
    f: &'s dyn Fn(&Self, Point3<f32>, f32) -> f32
}

impl<'s> SubVoxelCoverage<'s> {
    fn calc(&self, min_pos: Point3<f32>, sub_voxel_side: f32) -> f32 {
        (self.f)(self, min_pos, sub_voxel_side)
    }
}
