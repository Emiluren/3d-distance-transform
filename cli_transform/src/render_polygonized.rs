use glium::{glutin, implement_vertex, program, uniform};
use glium::Surface;
use glium::backend::Facade;
use glium::index::PrimitiveType;
use glium::draw_parameters::{BackfaceCullingMode, PolygonMode};
use glium::glutin::{
    Api, ControlFlow, ElementState, Event, GlProfile, GlRequest, KeyboardInput,
    VirtualKeyCode, WindowEvent
};
use nalgebra::{Isometry3, Point3, Perspective3, Vector3};
use isosurface::linear_hashed_marching_cubes::LinearHashedMarchingCubes;
use isosurface::source::HermiteSource;
use std::{f32, mem, slice};

// Partly based on this (Apache Licensed) example:
// https://github.com/swiftcoder/isosurface/blob/master/examples/torus.rs

#[derive(Copy, Clone)]
#[repr(C)]
struct Vertex {
    position: [f32; 3],
    normal: [f32; 3],
}

implement_vertex!(Vertex, position, normal);

type GraphicsBuffers = (glium::VertexBuffer<Vertex>, glium::IndexBuffer<u32>);

fn buffer_vertices<F: Facade>(display: &F, vertices: &Vec<f32>, indices: &Vec<u32>) -> GraphicsBuffers {
    let length_in_bytes = vertices.len() * mem::size_of::<f32>();
    let desired_length = length_in_bytes / mem::size_of::<Vertex>();
    let cast_vertices = unsafe {
        slice::from_raw_parts(vertices.as_ptr() as *const Vertex, desired_length)
    };

    let vertex_buffer: glium::VertexBuffer<Vertex> =
        glium::VertexBuffer::new(display, cast_vertices)
            .expect("failed to create vertex buffer");

    let index_buffer: glium::IndexBuffer<u32> =
        glium::IndexBuffer::new(display, PrimitiveType::TrianglesList, &indices)
            .expect("failed to create index buffer");

    (vertex_buffer, index_buffer)
}

// TODO: Split model generation into its own step
pub fn render(marching_cubes_source: &impl HermiteSource, center: Point3<f32>, marching_cubes_resolution: usize) {
    //let distance_map = Grid3D::read_from_file(input_file).unwrap();
    //let integer_distance = Grid3D::read_from_file("sphere.integer_distance").unwrap();

    println!("Generating mesh");
    let mut vertices = vec![];
    let mut indices = vec![];

    let mut linear_hashed_marching_cubes = LinearHashedMarchingCubes::new(marching_cubes_resolution);
    linear_hashed_marching_cubes.extract_with_normals(marching_cubes_source, &mut vertices, &mut indices);
    println!("Mesh generated");

    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new()
        .with_title("Distance transform")
        .with_dimensions(1024, 768);
    let context = glutin::ContextBuilder::new()
        .with_vsync(true)
        .with_gl_profile(GlProfile::Core)
        .with_gl(GlRequest::Specific(Api::OpenGl, (3, 3)))
        .with_depth_buffer(24);
    let display =
        glium::Display::new(window, context, &events_loop).expect("failed to create display");

    let (vertex_buffer, index_buffer) = buffer_vertices(&display, &vertices, &indices);

    let mut wireframe = false;

    let program = program!(
        &display,
        330 => {
            vertex: include_str!("shader.vert"),
            fragment: include_str!("shader.frag"),
        },
    ).expect("failed to compile shaders");

    let mut camera_angle = 0.0_f32;//f32::consts::PI / 4.0;

    let render = |camera_angle: f32, wireframe| {
        let mut surface = display.draw();
        surface.clear_color_and_depth((0.024, 0.184, 0.337, 0.0), 1.0);

        let camera_radius = 2.0;
        let camera_pos = center + Vector3::new(
            camera_angle.cos() * camera_radius,
            0.0,
            camera_angle.sin() * camera_radius
        );
        let view = Isometry3::look_at_rh(
            &camera_pos,
            &center,
            &Vector3::new(0.0, 1.0, 0.0),
        );

        let (view_w, view_h) = display.get_framebuffer_dimensions();
        let aspect = view_w as f32 / view_h as f32;
        let projection = Perspective3::new(aspect, f32::consts::PI / 4.0, 0.01, 1000.0);

        let uniforms = uniform! {
            model_view_projection: Into::<[[f32; 4]; 4]>::into(
                projection.as_matrix() * view.to_homogeneous()
            ),
        };

        let polygon_mode = if wireframe {
            PolygonMode::Line
        } else {
            PolygonMode::Fill
        };

        let draw_parameters = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::DepthTest::IfLess,
                write: true,
                ..Default::default()
            },
            point_size: Some(8.0),
            polygon_mode,
            backface_culling: BackfaceCullingMode::CullCounterClockwise,
            ..Default::default()
        };

        surface
            .draw(&vertex_buffer, &index_buffer, &program, &uniforms, &draw_parameters,)
            .expect("failed to draw to surface");

        surface.finish().expect("failed to finish rendering frame");
    };

    render(camera_angle, wireframe);
    events_loop.run_forever(|event| {
        match event {
            Event::WindowEvent { event, .. } => {
                match event {
                    WindowEvent::Closed => return ControlFlow::Break,
                    WindowEvent::KeyboardInput {
                        input:
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode,
                            ..
                        },
                        ..
                    } => match virtual_keycode {
                        Some(VirtualKeyCode::Escape) => return ControlFlow::Break,
                        Some(VirtualKeyCode::W) => {
                            wireframe = !wireframe;
                        }
                        Some(VirtualKeyCode::Left) => {
                            camera_angle -= 0.1;
                        }
                        Some(VirtualKeyCode::Right) => {
                            camera_angle += 0.1;
                        }
                        _ => (),
                    },
                    _ => (),
                }
                render(camera_angle, wireframe);
            },
            _ => (),
        }

        ControlFlow::Continue
    });
}
