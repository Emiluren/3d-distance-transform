#!/bin/bash

binary=target/release/distance_transform_3d
file="teapot_{$1}.h5"
$binary sample-obj teapot.obj --resolution $1 $file && \
    $binary transform $file && \
    $binary render-polygonized $file
