mod ffi;

use nalgebra::{Point3, Vector3};
use ndarray::{arr3, Array3, ArrayBase, Ix3, Zip};
use std::f32;

// TODO: Use builtin functionality of ndarray instead of ScanIterator
#[derive(Clone, Copy, Debug)]
enum Axis { X, Y, Z }

struct ScanIterator {
    limits: Vector3<i32>,
    axis: Axis,
    positive_dir: bool,
    current: Point3<i32>,
}

impl ScanIterator {
    fn new((axis, positive_dir): (Axis, bool), limits: Vector3<i32>) -> ScanIterator {
        let current = {
            if positive_dir {
                Point3::new(0, 0, 0)
            } else {
                match axis {
                    Axis::X => Point3::new(limits.x - 1, 0, 0),
                    Axis::Y => Point3::new(0, limits.y - 1, 0),
                    Axis::Z => Point3::new(0, 0, limits.z - 1),
                }
            }
        };
        ScanIterator { limits, axis, positive_dir, current }
    }
}

impl Iterator for ScanIterator {
    type Item = Point3<i32>;

    fn next(&mut self) -> Option<Self::Item> {
        let out_of_bounds =
            self.current.x < 0 || self.current.x >= self.limits.x ||
            self.current.y < 0 || self.current.y >= self.limits.y ||
            self.current.z < 0 || self.current.z >= self.limits.z;

        if out_of_bounds {
            return None;
        };

        // Swapping the axes means we can step them in a generic way
        let (mut swapped_point, swapped_limits) = {
            match self.axis {
                Axis::X => (self.current.yzx(), self.limits.yz()),
                Axis::Y => (self.current.zxy(), self.limits.zx()),
                Axis::Z => (self.current, self.limits.xy()),
            }
        };

        // Step position
        swapped_point.x += 1;
        if swapped_point.x >= swapped_limits.x {
            swapped_point.y += 1;
            swapped_point.x = 0;
        }
        if swapped_point.y >= swapped_limits.y {
            if self.positive_dir {
                swapped_point.z += 1;
            } else {
                swapped_point.z -= 1;
            }
            swapped_point.y = 0;
        }

        let old_current = self.current;

        // Swap axes back
        self.current = {
            match self.axis {
                Axis::X => swapped_point.zxy(),
                Axis::Y => swapped_point.yzx(),
                Axis::Z => swapped_point
            }
        };

        Some(old_current)
    }
}

// Solves a 3rd degree polynominal equation and returns the solution that
// matches one of the expected ones
// Taken from this page https://www.easycalculation.com/algebra/cubic-equation.php
fn cardan_roots([a, b, c, d]: [f32; 4], (min_limit, max_limit): (f32, f32)) -> f32 {
    let (b, c, d) = (b / a, c / a, d / a);

    let q = (3.0*c - (b*b))/9.0;
    let r = (-(27.0*d) + b*(9.0*c - 2.0*(b*b))) / 54.0;
    let disc = q*q*q + r*r;

    let term1 = b / 3.0;
    if disc > 0.0 { // one root real, two are complex
        let s = (r + disc.sqrt()).cbrt();
        let t = (r - disc.sqrt()).cbrt();
        return (s + t) / 2.0;
    } 

    // The remaining options are all real
    if disc == 0.0 { // All roots real, at least two are equal.
        let r13 = r.cbrt();
        let x = -term1 + 2.0*r13;

        if min_limit <= x && x <= max_limit {
            return x;
        }
        return -r13 - term1;
    }

    // Only option left is that all roots are real and unequal (to get here, q < 0)
    let q = -q;
    let dum1 = (r/(q*q*q).sqrt()).acos();
    let r13 = 2.0 * q.sqrt();
    let x = -term1 + r13*(dum1/3.0).cos();
    if min_limit <= x && x <= max_limit {
        return x;
    }
    let x = -term1 + r13*((dum1 + 2.0*f32::consts::PI)/3.0).cos();
    if min_limit <= x && x <= max_limit {
        return x;
    }
    return -term1 + r13*((dum1 + 4.0*f32::consts::PI)/3.0).cos();
}

// From "3D voxel coverage distance transforms"
// Input: normal direction of the boundary (nx, ny, nz)
// voxel coverage, (0 <= coverage <= 1.0)
// Output: signed distance from the voxel center to the object boundary
fn edgedf(dir: Vector3<f32>, coverage: f32) -> f32 {
    if coverage > 0.5 {
        return -edgedf(dir, 1.0 - coverage);
    }

    // with nx >= ny >= nz and
    let mut dir_array = [dir.x.abs(), dir.y.abs(), dir.z.abs()];
    dir_array.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let n = Vector3::new(dir_array[2], dir_array[1], dir_array[0]);

    // implies n.z == 0
    if n.y == 0.0 {
        return 0.5 - coverage;
    }

    // Depending on the edges of the voxel that the normal plane intersects the
    // coverage will have different sizes
    let v1 = n.z*n.z / (6.0*n.x*n.y);
    if coverage <= v1 {
        return (n.x + n.y + n.z)/2.0 - (6.0*coverage*n.x*n.y*n.z).cbrt();
    }

    let v2 = (n.z*n.z + 3.0*n.y*n.z + 3.0*n.y*n.y)/(6.0*n.x*n.y);
    if coverage <= v2 {
        return (n.x + n.y)/2.0 - (2.0*coverage*n.x*n.y - n.z*n.z/12.0).sqrt();
    }

    let v3 = (n.z*n.z - 3.0*n.x*n.z + 3.0*n.x*n.x)/(6.0*n.x*n.y) -
        (n.x - n.y).powi(3)/(6.0*n.x*n.y*n.z);

    let x = if coverage <= v3 && n.z != 0.0 {
        let polynomial = [
            n.z.powi(3)/n.y.powi(3),
            -3.0*(n.y + n.z)*n.z.powi(3)/(n.x*n.y.powi(3)),
            3.0*n.z.powi(3)*(n.y*n.y + n.z*n.z)/(n.x*n.x*n.y.powi(3)),
            6.0*coverage*n.z.powi(4)/(n.x*n.x*n.y*n.y) -
                n.z.powi(3)*(n.y.powi(3) + n.z.powi(3))/(n.x.powi(3)*n.y.powi(3))
        ];

        if n.y + n.z <= n.x {
            cardan_roots(polynomial, (n.y/n.x, (n.y+n.z)/n.x))
        } else {
            cardan_roots(polynomial, (n.y/n.x, 1.0))
        }
    } else if n.y + n.z <= n.x {
        coverage + (n.y + n.z) / (2.0*n.x)
    } else {
        let polynomial = [
            2.0*n.z.powi(3)/n.y.powi(3),
            -3.0*n.z.powi(3)/n.y.powi(3)*(n.x + n.y + n.z)/n.x,
            3.0*n.z.powi(3)/n.y.powi(3)*(n.x*n.x + n.y*n.y + n.z*n.z)/(n.x*n.x),
            6.0*coverage*n.z.powi(4)/(n.x*n.x*n.y*n.y) -
                n.z.powi(3)/n.y.powi(3)*(n.x.powi(3) + n.y.powi(3) + n.z.powi(3))/n.z.powi(3)
        ];

        cardan_roots(polynomial, (1.0, (n.y + n.z)/n.x))
    };
    return (n.x + n.y + n.z)/2.0 - x*n.x;
}

pub fn distance_transform<FloatArray, Vec3Array, F>(
    input: &ArrayBase<FloatArray, Ix3>, gradient: &ArrayBase<Vec3Array, Ix3>, coverage_to_distance: F
) -> (Array3<f32>, Array3<Vector3<i32>>) where
    FloatArray: ndarray::Data<Elem = f32>,
    Vec3Array: ndarray::Data<Elem = Vector3<f32>>,
    F: Fn(Vector3<i32>, Vector3<f32>, f32, f32) -> f32
{
    // Start by setting all empty elements to 0 and filled ones to f64::MAX
    let initial_dist = |coords| {
        let coverage = input[coords];

        if coverage == 1.0 {
            f32::NEG_INFINITY
        } else if coverage > 0.0 {
            let grad = gradient[coords].normalize();
            coverage_to_distance(Vector3::new(0, 0, 0), grad, coverage, 0.)
        } else {
            f32::INFINITY
        }
    };

    let mut distance_map = Array3::from_shape_fn(input.dim(), initial_dist);
    let mut integer_distance = Array3::from_elem(input.dim(), Vector3::new(0, 0, 0));

    let y_mask = vec![
        Vector3::new(-1, 0, -1), Vector3::new(0, 0, -1), Vector3::new(1, 0, -1),
        Vector3::new(-1, 0, 0), Vector3::new(0, 0, 0), Vector3::new(1, 0, 0),
        Vector3::new(-1, 0, 1), Vector3::new(0, 0, 1), Vector3::new(1, 0, 1),
    ];
    let x_mask: Vec<_> = y_mask.iter().map(|v| Vector3::new(0, v.z, v.x)).collect();
    let z_mask: Vec<_> = y_mask.iter().map(|v| Vector3::new(v.x, v.z, 0)).collect();

    let scan_directions = [
        (Axis::X, true),
        (Axis::X, false),
        (Axis::Y, true),
        (Axis::Y, false),
        (Axis::Z, true),
        (Axis::Z, false),
    ];

    let input_size = Vector3::new(input.dim().0 as i32, input.dim().1 as i32, input.dim().2 as i32);

    let in_bounds =
        |v: Point3<i32>|
            (0 .. input_size.x).contains(&v.x) &&
            (0 .. input_size.y).contains(&v.y) &&
            (0 .. input_size.z).contains(&v.z);

    let mut changes_made = true;
    #[cfg(feature = "log_progress")]
    let mut iteration = 1;
    while changes_made {
        changes_made = false;

        #[cfg(feature = "log_progress")] {
            println!("DT: iteration {}", iteration);
            iteration += 1;
        }

        for &(axis, positive_dir) in &scan_directions {
            let scan = ScanIterator::new((axis, positive_dir), input_size);
            let dir_factor = if positive_dir { 1 } else { -1 };
            let (mask, axis_vector) = match axis {
                Axis::X => (&x_mask, Vector3::x() * dir_factor),
                Axis::Y => (&y_mask, Vector3::y() * dir_factor),
                Axis::Z => (&z_mask, Vector3::z() * dir_factor),
            };
            for pos in scan {
                for offset in mask.iter().map(|v| v - axis_vector) {
                    let neighbor = pos + offset;

                    // Filter out out of bounds accesses
                    if !in_bounds(neighbor) {
                        continue;
                    }

                    fn into_tuple(v: Point3<i32>) -> (usize, usize, usize) {
                        (v.x as usize, v.y as usize, v.z as usize)
                    }

                    let neighbor_edge_dist = integer_distance[into_tuple(neighbor)];
                    let edge = neighbor + neighbor_edge_dist;
                    let coverage = input[into_tuple(edge)];

                    // Skip elements where the integer edge distance does not
                    // point to an actual edge
                    if coverage == 0.0 || coverage == 1.0 {
                        continue;
                    }

                    let new_i_dist = neighbor_edge_dist + offset;
                    let gradient_normal = gradient[into_tuple(edge)].normalize();
                    let new_dist = coverage_to_distance(new_i_dist, gradient_normal, coverage, distance_map[into_tuple(pos)]);

                    // if pos == Point3::new(4, 4, 0) {
                    //     println!("new_dist = {}, 0.5 - coverage = {}", new_dist, 0.5 - coverage);
                    // }

                    if new_dist.abs() < distance_map[into_tuple(pos)].abs() {
                        distance_map[into_tuple(pos)] = new_dist;
                        integer_distance[into_tuple(pos)] = new_i_dist;
                        changes_made = true;
                    }
                }
            }
        }
    }

    (distance_map, integer_distance)
}

pub trait SampleArray {
    fn calculate_gradient(&self) -> Array3<Vector3<f32>>;
    fn downsample_average(&self, chunk_size: usize) -> Array3<f32>;
}

impl<S: ndarray::Data<Elem = f32>> SampleArray for ArrayBase<S, Ix3> {
    fn calculate_gradient(&self) -> Array3<Vector3<f32>> {
        let x_grad_kernel = arr3(&[
            [[-1.0, -2.0, -1.0], [-2.0, -4.0, -2.0], [-1.0, -2.0, -1.0]],
            [[ 0.0,  0.0,  0.0], [ 0.0,  0.0,  0.0], [ 0.0,  0.0,  0.0]],
            [[ 1.0,  2.0,  1.0], [ 2.0,  4.0,  2.0], [ 1.0,  2.0,  1.0]],
        ]);

        let y_grad_kernel = x_grad_kernel.clone().permuted_axes([1, 0, 2]);
        let z_grad_kernel = x_grad_kernel.clone().permuted_axes([2, 1, 0]);

        // TODO: Does not work due to wrong dimensions
        // let mut res = Array3::zeros(self.dim());

        // Zip::from(&mut res).and(self.windows((3, 3, 3))).apply(
        //     |res, w| *res = Vector3::new(
        //         (&w * &x_grad_kernel).sum(),
        //         (&w * &y_grad_kernel).sum(),
        //         (&w * &z_grad_kernel).sum(),
        //     )
        // );
        // res

        let (size_x, size_y, size_z) = self.dim();

        Array3::from_shape_fn(
            self.dim(),
            |(x, y, z)| {
                let mut val = Vector3::new(0.0, 0.0, 0.0);
                let clamp_pos = |(px, py, pz): (isize, isize, isize)| (
                    px.max(0).min(size_x as isize - 1) as usize,
                    py.max(0).min(size_y as isize - 1) as usize,
                    pz.max(0).min(size_z as isize - 1) as usize,
                );
                for kz in -1 ..= 1 {
                    for ky in -1 ..= 1 {
                        for kx in -1 ..= 1 {
                            let offset_pos = clamp_pos((x as isize + kx, y as isize + ky, z as isize + kz));
                            let kernel_val = |kernel: &Array3<f32>|
                                kernel[((kx + 1) as usize, (ky + 1) as usize, (kz + 1) as usize)];

                            val += Vector3::new(
                                self[offset_pos] * kernel_val(&x_grad_kernel),
                                self[offset_pos] * kernel_val(&y_grad_kernel),
                                self[offset_pos] * kernel_val(&z_grad_kernel)
                            );
                        }
                    }
                }
                val

            }
        )
    }

    fn downsample_average(&self, chunk_size: usize) -> Array3<f32> {
        let (size_x, size_y, size_z) = self.dim();
        let mut res = Array3::zeros((
            size_x / chunk_size,
            size_y / chunk_size,
            size_z / chunk_size
        ));
        Zip::from(&mut res).and(
            self.exact_chunks((chunk_size, chunk_size, chunk_size))
        ).apply(|res, chunk| *res = chunk.sum() / (chunk_size as f32).powi(3));
        res / chunk_size as f32
    }
}

pub fn default_coverage_to_distance(new_i_dist: Vector3<i32>, gradient_normal: Vector3<f32>, coverage: f32, old_distance: f32) -> f32 {
    let float_new_i_dist = new_i_dist.map(|c| c as f32);

    // For edge elements we can do a more accurate calculation based on the gradient
    let gradient_distance = edgedf(gradient_normal, coverage);

    if gradient_distance.abs() <= 0.75_f32.sqrt() && float_new_i_dist.cross(&gradient_normal).magnitude() <= 0.5 {
        float_new_i_dist.dot(&gradient_normal) + gradient_distance
    } else {
        float_new_i_dist.magnitude() * old_distance.signum() +
            edgedf(float_new_i_dist.normalize(), coverage) // GIVES WRONG RESULT WHEN n = (0, 0, 0)
    }
}
