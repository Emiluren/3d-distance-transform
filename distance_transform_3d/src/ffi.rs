#[cfg(feature = "c_ffi")]
#[no_mangle]
pub extern "C" fn default_coverage_to_distance(new_i_dist: IVec3, gradient_normal: Vec3, coverage: f32, old_distance: f32) -> f32 {
    let new_i_dist = Vector3::new(new_i_dist.x, new_i_dist.y, new_i_dist.z);
    let gradient_normal = Vector3::new(gradient_normal.x, gradient_normal.y, gradient_normal.z);
    crate::default_coverage_to_distance(new_i_dist, gradient_normal, coverage, old_distance)
}

#[cfg(feature = "c_ffi")]
#[repr(C)]
pub struct Vec3 {
    x: f32,
    y: f32,
    z: f32,
}

#[cfg(feature = "c_ffi")]
impl From<Vector3<f32>> for Vec3 {
    fn from(v: Vector3<f32>) -> Self {
        Vec3 { x: v.x, y: v.y, z: v.z }
    }
}

#[cfg(feature = "c_ffi")]
#[repr(C)]
pub struct IVec3 {
    x: i32,
    y: i32,
    z: i32,
}

#[cfg(feature = "c_ffi")]
impl From<Vector3<i32>> for IVec3 {
    fn from(v: Vector3<i32>) -> Self {
        IVec3 { x: v.x, y: v.y, z: v.z }
    }
}

#[cfg(feature = "c_ffi")]
#[no_mangle]
pub unsafe extern "C" fn distance_transform_3d(
    size_x: libc::size_t,
    size_y: libc::size_t,
    size_z: libc::size_t,
    coverage_to_distance: extern "C" fn(integer_distance: IVec3, gradient: Vec3, coverage: f32, old_distance: f32) -> f32,
    coverage_data: *const libc::c_float,
    distance_buffer: *mut libc::c_float,
) {
    let coverage = ndarray::ArrayView::from_shape_ptr(
        (size_x, size_y, size_z),
        coverage_data
    );
    let gradient = coverage.calculate_gradient();

    let (distance_map, _integer_distance) =
        crate::distance_transform(
            &coverage,
            &gradient,
            |i, g, c, o| coverage_to_distance(i.into(), g.into(), c, o)
        );

    std::intrinsics::copy_nonoverlapping(
        distance_map.as_ptr(),
        distance_buffer,
        size_x * size_y * size_z
    );
}
