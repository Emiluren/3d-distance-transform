#include "distance_transform_3d.h"
#include <stdio.h>
#include <math.h>

const int MARGIN = 1;

/* Calculates coverage data for a sphere with a radius of size / 2 - MARGIN.
 */
void calculate_sphere_coverage(const unsigned int size, float buffer[size][size][size]) {
    float radius = size / 2.0f - MARGIN;
    const unsigned int sub_size = 8;
    const float dv = powf((float)sub_size, -3.0f);

    for (unsigned int x = MARGIN; x < size - MARGIN; x++) {
        for (unsigned int y = MARGIN; y < size - MARGIN; y++) {
            for (unsigned int z = MARGIN; z < size - MARGIN; z++) {
                // Integrate numerically over cube/sphere intersection
                float volume = 0;
                for (unsigned int xx = 0; xx < sub_size; xx++) {
                    for (unsigned int yy = 0; yy < sub_size; yy++) {
                        for (unsigned int zz = 0; zz < sub_size; zz++) {
                            float x_coord = x + xx / (float)sub_size;
                            float y_coord = y + yy / (float)sub_size;
                            float z_coord = z + zz / (float)sub_size;

                            float x_diff = x_coord - size/2.0f;
                            float y_diff = y_coord - size/2.0f;
                            float z_diff = z_coord - size/2.0f;

                            if (x_diff*x_diff + y_diff*y_diff + z_diff*z_diff < radius*radius) {
                                volume += dv;
                            }
                        }
                    }
                }
                buffer[x][y][z] = volume;
            }
        }
    }
}

void print_cube_data(const unsigned int size, const float data[size][size][size]) {
    for (unsigned int z = 0; z < size; z++) {
        printf("z = %u\n", z);
        for (unsigned int y = 0; y < size; y++) {
            for (unsigned int x = 0; x < size; x++) {
                printf("% 5.1f ", data[x][y][z]);
            }
            printf("\n");
        }
        printf("\n");
    }
}

const unsigned int SPHERE_SIZE = 10;

int main() {
    const size_t size_x = SPHERE_SIZE;
    const size_t size_y = SPHERE_SIZE;
    const size_t size_z = SPHERE_SIZE;

    float coverage_buffer[SPHERE_SIZE][SPHERE_SIZE][SPHERE_SIZE];
    for (unsigned int x = 0; x < SPHERE_SIZE; x++) {
        for (unsigned int y = 0; y < SPHERE_SIZE; y++) {
            for (unsigned int z = 0; z < SPHERE_SIZE; z++) {
                coverage_buffer[x][y][z] = 0.0f;
            }
        }
    }
    calculate_sphere_coverage(SPHERE_SIZE, coverage_buffer);

    printf("Sphere coverage:\n");
    print_cube_data(SPHERE_SIZE, coverage_buffer);

    printf("Calculating distance map...\n");
    float distance_buffer[SPHERE_SIZE][SPHERE_SIZE][SPHERE_SIZE];
    distance_transform_3d(
        size_x,
        size_y,
        size_z,
        default_coverage_to_distance,
        &coverage_buffer[0][0][0],
        &distance_buffer[0][0][0]
    );

    printf("Sphere distance map:\n");
    print_cube_data(SPHERE_SIZE, distance_buffer);
}
