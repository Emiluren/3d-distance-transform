#include <stddef.h>

struct Vector3f {
    float x, y, z;
};

struct Vector3i {
    int x, y, z;
};

void distance_transform_3d(
    size_t size_x,
    size_t size_y,
    size_t size_z,
    float (*coverage_to_distance)(
        struct Vector3i integer_distance,
        struct Vector3f gradient_normal,
        float coverage,
        float old_distance
    ),
    const float *coverage_data,
    float *distance_buffer
);

float default_coverage_to_distance(
    struct Vector3i integer_distance,
    struct Vector3f gradient_normal,
    float coverage,
    float old_distance
);
